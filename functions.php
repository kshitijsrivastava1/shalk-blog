<?php
/**
 * SHALK Blog functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package SHALK_Blog
 */

if ( ! function_exists( 'shalk_blog_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function shalk_blog_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on SHALK Blog, use a find and replace
		 * to change 'shalk-blog' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'shalk-blog', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'shalk-blog' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'shalk_blog_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 50,
			'width'       => 100,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'shalk_blog_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function shalk_blog_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'shalk_blog_content_width', 640 );
}
add_action( 'after_setup_theme', 'shalk_blog_content_width', 0 );

function shalk_blog_default_header_image(){
		$theme_url = get_stylesheet_directory_uri();
		return $theme_url . '/images/banner.jpg';
}


/**
 * Load SHALK_Blog_Search_form Class File.
 */
require get_template_directory() . '/inc/shalk-blog-widget.php';


/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function shalk_blog_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'shalk-blog' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'shalk-blog' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Section 1', 'shalk-blog' ),
		'id'            => 'footer-section-sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'shalk-blog' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Section 2', 'shalk-blog' ),
		'id'            => 'footer-section-sidebar-2',
		'description'   => esc_html__( 'Add widgets here.', 'shalk-blog' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Section 3', 'shalk-blog' ),
		'id'            => 'footer-section-sidebar-3',
		'description'   => esc_html__( 'Add widgets here.', 'shalk-blog' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Right Sidebar', 'shalk-blog' ),
		'id'            => 'right-sidebar',
		'description'   => esc_html__( 'Add widgets here.', 'shalk-blog' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'shalk_blog_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function shalk_blog_scripts() {

	$theme_url = get_stylesheet_directory_uri();
	wp_enqueue_style( 'shalk-blog-fontawesome-min', $theme_url . '/css/fontawesome.min.css' );
	wp_enqueue_style( 'shalk-blog-bootstrap', $theme_url . '/css/bootstrap.css' );
	wp_enqueue_style( 'shalk-blog-style', get_stylesheet_uri() );
	wp_enqueue_style( 'shalk-blog-fonts-googleapis', '//fonts.googleapis.com/css?family=Oswald:400,300,700' );

	wp_enqueue_script( 'shalk-blog-jquery', $theme_url . '/js/jquery-1.11.1.min.js', array(), false, false );
	wp_enqueue_script( 'shalk-blog-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'shalk-blog-bootstrap', $theme_url . '/js/bootstrap.js', array(), false, true );
	wp_enqueue_script( 'shalk-blog-theme_ks', $theme_url . '/js/theme_ks.js', array(), false, false );
	if (is_home()) {
		wp_enqueue_script( 'shalk-blog-responsiveslides', $theme_url . '/js/responsiveslides.min.js', array(), false, true );
		wp_enqueue_script( 'shalk-blog-jquery-marquee', $theme_url . '/js/jquery.marquee.js', array(), false, false );

	}
	wp_enqueue_script( 'shalk-blog-bootstrap', $theme_url . '/js/bootstrap.js', array(), false, false );

	wp_enqueue_script( 'shalk-blog-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'shalk_blog_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}

/**
 * Code To Disable Gutenberg Editor.
 */
require get_template_directory() . '/disable-gutenberg-editor.php';
