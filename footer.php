<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SHALK_Blog
 */

?>

	</div><!-- #content -->
	<?php
		do_action( 'shalk_blog_before_footer' );
	?>

<!-- footer -->
	<div class="footer-top-w3layouts-agile">
		<div class="container">
			<p>at least 150 missing and there dead in landslide after monsoon 
				rains in central Sri Lanka, officials say <a href="#">http//example.com</a></p>
		</div>
	</div>
	<footer id="colophon" class="site-footer footer">
		<div class="container">
			<div class="footer-grids wthree-agile">
				<div class="col-md-4 footer-grid-left">
					<?php
						dynamic_sidebar( 'footer-section-sidebar-1' );
					?>
				</div>
				<div class="col-md-4 footer-grid-left">
					<?php
						dynamic_sidebar( 'footer-section-sidebar-2' );
					?>
				</div>
				<div class="col-md-4 footer-grid-left">
					<?php
						dynamic_sidebar( 'footer-section-sidebar-3' );
					?>
				</div>
				<!-- <div class="col-md-4 footer-grid-left">
					<h3>twitter feed</h3>
					<ul>
						<li><a href="#">the moment an unlimited #antares rocket exploded seconds after launch 
							<i>http//example.com</i></a><span>15 minutes ago</span></li>
						<li><a href="mailto:info@example.com" class="cols">@NASA</a> & <a href="mailto:info@example.com" class="cols">
							@orbital science</a> <a href="#">gathering data on failure #antares rocket bound 
							for international space</a><span>45 minutes ago</span></li>
						<li><a href="#">ex-cabinet minister chris huhne loses legal challenge over $77,750 
							court costs incurred in speeding points</a><span>1 day ago</span></li>
					</ul>
				</div>
				<div class="col-md-4 footer-grid-left">
					<h3>contact form</h3>
					<form>
						<input type="text" value="enter your full name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'enter your full name';}" required="">
						<input type="email" value="enter your email address" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'enter your email address';}" required="">
						<textarea onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message...';}" required="">Message...</textarea>
						<input type="submit" value="Submit" >
					</form>
				</div>
				<div class="col-md-4 footer-grid-left">
					<h3>about us</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
						eiusmod tempor incididunt ut labore et dolore magna aliqua. 
						Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.
						<span>But I must explain to you how all this mistaken idea of denouncing
						pleasure and praising pain was born and I will give you a complete 
						account of the system, and expound the actual teachings of the 
						great explorer.</span>
						<i>- The Entire TLG Team</i></p>
				</div> -->
				<div class="clearfix"> </div>
			</div>
			<div class="footer-bottom">
				<div class="site-info footer-bottom-left-whtree-agileinfo">
					<p>
						<a href="<?php echo esc_url( __( 'https://shalksoftech.com/', 'shalk-blog' ) ); ?>">
							<?php
							/* translators: %s: CMS name, i.e. WordPress. */
							printf( esc_html__( 'Proudly powered by %s', 'shalk-blog' ), 'WordPress' );
							?>
						</a>
						<span class="sep"> | </span>
							<?php
							/* translators: 1: Theme name, 2: Theme author. */
							printf( esc_html__( 'Theme: %1$s by %2$s.', 'shalk-blog' ), 'shalk-blog', '<a href="https://in.linkedin.com/in/kshitij-srivastava-79590415b">Kshitij Srivastava</a>' );
							?>
					</p>
					<!-- <p>&copy 2019 SHALK Blog. All rights reserved | Template by <a href="https://shalksoftech.com/">Kshitij Srivastava.</a></p> -->
				</div>
				<div class="footer-bottom-right-whtree-agileinfo">
					<ul>
						<li><a href="#" class="icon-button twitter"><i class="icon-twitter"></i><span></span></a></li>
						<li><a href="#" class="icon-button google"><i class="icon-google"></i><span></span></a></li>
						<li><a href="#" class="icon-button v"><i class="icon-v"></i><span></span></a></li>
					</ul>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</footer>
<!-- //footer -->
</div><!-- #page -->

<?php wp_footer(); ?>
<?php
	do_action( 'shalk_blog_after_footer' );
?>

</body>
</html>
