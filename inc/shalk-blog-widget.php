<?php

/**
 * Register widgets.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */

Class Register_SHALK_Blog_Widget{
	public function __construct(){
	}

	public static function register_widget(){
		register_widget( 'SHALK_Blog_Search_form' );
		register_widget( 'SHALK_Blog_Recent_tags' );		
	}
}

add_action( 'widgets_init', array('Register_SHALK_Blog_Widget' , 'register_widget'));


if ( ! class_exists( 'SHALK_Blog_Search_form' ) ) :
	class SHALK_Blog_Search_form extends WP_Widget {
		// class constructor
		public function __construct() {
			$widget_ops = array( 
				'classname' => 'SHALK_Blog_Search_form',
				'description' => 'A widget for SHALK blog readers',
			);
			parent::__construct( 'shalk_blog_search_form', 'SHALK Blog Search form', $widget_ops );
		}
		
		// output the widget content on the front-end
		public function widget( $args, $instance ) {
		?>
			<div class="news-grid-rght2">
				<?php
					echo $args['before_widget'];
					if ( ! empty( $instance['title'] ) ) {
						echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] )  . $args['after_title'];
					}
					$search_keyword = isset($_REQUEST['s']) ? $_REQUEST['s'] : "";
				?>
				<form>
					<input type="text" name="s" placeholder="Search..." value="<?php echo $search_keyword; ?>">
					<input type="submit" value="Submit">
				</form>
			</div>
		<?php
		}

		// output the option form field in admin Widgets screen
		public function form( $instance ) {
			$title = ! empty( $instance['title'] ) ? $instance['title'] : "";
			?>
			<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
			<?php esc_attr_e( 'Title:', 'text_domain' ); ?>
			</label> 
			
			<input 
				class="widefat" 
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" 
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" 
				type="text" 
				value="<?php echo esc_attr( $title ); ?>">
			</p>
			<?php
		}

		// save options
		public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : strip_tags( $old_instance['title'] );
				
			$selected_posts = ( ! empty ( $new_instance['selected_posts'] ) ) ? (array) $new_instance['selected_posts'] : array();
			$instance['selected_posts'] = array_map( 'sanitize_text_field', $selected_posts );

			return $instance;
		}
	}
endif;


if ( ! class_exists( 'SHALK_Blog_Recent_tags' ) ) :
	class SHALK_Blog_Recent_tags extends WP_Widget {
		// class constructor
		public function __construct() {
			$widget_ops = array( 
				'classname' => 'SHALK_Blog_Recent_tags',
				'description' => 'This widget for show the latest tags',
			);
			parent::__construct( 'shalk_blog_recent_tags', 'SHALK Blog Recent tags', $widget_ops );
		}
		
		// output the widget content on the front-end
		public function widget( $args, $instance ) {
			$total_tags = isset($instance['total_tags']) ? $instance['total_tags'] : 5;
			$tags = get_tags( array(
							    'number' => $total_tags,
							    'orderby' => 'count',
							    'order' => 'DESC'
							) );
			if ( count( $tags ) ) :
		?>
				<div class="footer-top-grid1">
					<?php
						if ( ! empty( $instance['title'] ) ) {
							echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] )  . $args['after_title'];
						}
					?>
					<ul class="tag2">
						<?php
							foreach ( $tags as $tag ) {
							    $tag_link = get_tag_link( $tag->term_id );  
							    $html = "<li>";
							    $html .= "<a href='{$tag_link}' title='{$tag->name}' class='{$tag->slug}'>";
							    $html .= "{$tag->name}</a>";
							    $html .= "</li>";
							    echo $html;
							}
						?>
					</ul>
				</div>
		<?php
			endif;
		}

		// output the option form field in admin Widgets screen
		public function form( $instance ) {
			$title = ! empty( $instance['title'] ) ? $instance['title'] : "";
			$total_tags = ! empty( $instance['total_tags'] ) ? $instance['total_tags'] : 5;
			?>
			<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
			<?php esc_attr_e( 'Title:', 'text_domain' ); ?>
			</label> 
			
			<input 
				class="widefat" 
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" 
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" 
				type="text" 
				value="<?php echo esc_attr( $title ); ?>">
			</p>
			<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'total_tags' ) ); ?>">
			<?php esc_attr_e( 'Total Tags:', 'text_domain' ); ?>
			</label> 
			
			<input 
				class="widefat" 
				id="<?php echo esc_attr( $this->get_field_id( 'total_tags' ) ); ?>" 
				name="<?php echo esc_attr( $this->get_field_name( 'total_tags' ) ); ?>" 
				type="number" 
				value="<?php echo esc_attr( $total_tags ); ?>">
			</p>
			<?php
		}

		// save options
		public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : strip_tags( $old_instance['title'] );
			$instance['total_tags'] = ( ! empty( $new_instance['total_tags'] ) ) ? strip_tags( $new_instance['total_tags'] ) : strip_tags( $old_instance['total_tags'] );
				
			$selected_posts = ( ! empty ( $new_instance['selected_posts'] ) ) ? (array) $new_instance['selected_posts'] : array();
			$instance['selected_posts'] = array_map( 'sanitize_text_field', $selected_posts );

			return $instance;
		}
	}
endif;

?>
