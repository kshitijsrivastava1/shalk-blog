<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SHALK_Blog
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<header id="masthead" class="site-header">
		<div class="main-header data-bg site-branding">
			<?php
				$banner_class_no = 1;
				if(is_home()){
					$banner_class_no = "";
				}
				$header_image = get_header_image();
			?>
			<!-- banner -->
				<div class="banner<?php echo $banner_class_no; ?>" data-bgks="<?php echo $header_image != '' ? $header_image : shalk_blog_default_header_image(); ?>">
					<div class="banner-info<?php echo $banner_class_no; ?>">
						<div class="container">
							<nav class="navbar navbar-default">
								<!-- Brand and toggle get grouped for better mobile display -->
								<div class="navbar-header">
								  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								  </button>
									<div class="logo">
										<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>"rel="home">
											<?php
												$custom_logo_id = get_theme_mod( 'custom_logo' );
												$custom_logo_url = wp_get_attachment_image_url( $custom_logo_id , 'full' );
												?>
												<span>
													<img src="<?php echo  esc_url( $custom_logo_url ); ?>" class="custom-logo" alt="<?php bloginfo( 'name' ); ?>">
												</span>
												<?php
													echo bloginfo( 'name' );
												?>
										</a>
										<?php
											// the_custom_logo();
										?>
									</div>
								</div>

								<!-- Collect the nav links, forms, and other content for toggling -->
								<!-- <div class="collapse navbar-collapse nav-wil"> -->
									<!-- <ul class="nav navbar-nav"> -->
									<?php 
										$defaults = array(
										    'theme_location'  => 'menu-1',
										    'menu'            => 'primary',
										    'container'       => 'div',
										    'container_class' => 'collapse navbar-collapse nav-wil',
										    'container_id'    => 'bs-example-navbar-collapse-1',
										    'menu_class'      => 'nav navbar-nav',
										    'menu_id'         => 'nav',
										    'echo'            => true,
										    'fallback_cb'     => 'wp_page_menu',
										    'before'          => '',
										    'after'           => '',
										    'link_before'     => '',
										    'link_after'      => '',
										    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
										    'depth'           => 0,
										    'walker'          => ''
										);
									?>
									<?php
										wp_nav_menu( 
											$defaults
										); 
									?>
										<!-- <li class="act"><a href="index.html" class="effect1 active">Home</a></li>
										<li><a href="events.html">Reviews</a></li>
										<li><a href="breaking.html">Culture</a></li>
										<li><a href="entertainment.html">Entertainment</a></li>
										<li role="presentation" class="dropdown">
											<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
											  Business <span class="caret"></span>
											</a>
											<ul class="dropdown-menu">
											  <li><a href="short-codes.html">Short-Codes</a></li>
											  <li><a href="icons.html">Icons</a></li>
											 
											</ul>
										</li>
										<li><a href="contact.html">Contact Us</a></li> -->
									<!-- </ul> -->
								<!-- </div> -->
								<!-- /.navbar-collapse -->	
								
							</nav>
							<?php
								if ( is_singular() ) :
									the_title( '<h1 class="page-title">', '</h1>' );
								endif;
								if ( is_archive() ) :
									the_archive_title( '<h1 class="page-title">', '</h1>' );
									the_archive_description( '<div class="archive-description">', '</div>' );
								endif;
							?>
							<?php
								if (is_home()) {
							?>
							<!--banner-Slider-->
								<script>
									// You can also use "$(window).load(function() {"
									$(function () {
										// Slideshow 4
										$("#slider3").responsiveSlides({
										  	auto: true,
										  	pager: true,
										  	nav:true,
											speed: 500,
											namespace: "callbacks",
											before: function () {
											  $('.events').append("<li>before event fired.</li>");
											},
											after: function () {
											  $('.events').append("<li>after event fired.</li>");
											}
										});
									});
								</script>
								<div  id="top" class="callbacks_container">
									<ul class="rslides" id="slider3">
										<?php
											$slider_post_args = array(
																	'numberposts'	=> 4,
																	'orderby'		=> 'date',
															        'order'			=> 'DESC',
															        'post_type'		=> 'post',
															        'post_status'	=> 'publish',
															        '$post'			=> 0,
																);
											$slider_posts = get_posts( $slider_post_args );
												// print_r("<pre>");
												// print_r($slider_posts);
												// die();
											foreach ($slider_posts as $key => $post) {
										?>
												<li>
													<div class="banner-info-slider">
														<ul>
															<li><a href="single.html">Blogger</a></li>
															<li><?php echo date("d-M-Y" , strtotime($post->post_date)); ?></li>
														</ul>
														<h3><?php echo $post->post_title; ?></h3>
														<p>
															<?php 
																echo $post->post_excerpt !="" ? $post->post_excerpt : substr(strip_tags($post->post_content) , 0 , 100);
															?>
															<span>By <i><?php echo get_the_author_meta( 'display_name', $user_id = $post->post_author ); ?></i></span></p>
														<div class="more">
															<a href="<?php echo get_permalink( $post->ID ); ?>" class="type-1">
																<span> Read More </span>
																<span> Read More </span>
															</a>
														</div>
													</div>
												</li>
										<?php
											}
										?>
									</ul>
								</div>
							<?php
								}
							?>
						</div>
					</div>
				</div>
			<!-- banner -->
		</div>
	</header>
	<?php
		do_action( 'shalk_blog_after_header' );
	?>

	<div id="content" class="site-content">
	<?php
		do_action( 'shalk_blog_inner_site_content' );
	?>
