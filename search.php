<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package SHALK_Blog
 */

get_header();
?>
<div class="container">
	<div class="single-grid">
		<div class="col-md-9 blog-left">
			<div class="blog-left-grid">
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
						<?php if ( have_posts() ) : ?>

							<header class="page-header search-page-header">
								<h1 class="search-page-title">
									<i class="fa fa-search"></i>
									<?php
									/* translators: %s: search query. */
									printf( esc_html__( 'Search Results for: %s', 'shalk-blog' ), '<span>' . get_search_query() . '</span>' );
									?>
								</h1>
							</header>
							<!-- .page-header -->

							<?php
							/* Start the Loop */
							while ( have_posts() ) :
								the_post();

								/*
								 * Include the Post-Type-specific template for the content.
								 * If you want to override this in a child theme, then include a file
								 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
								 */
								get_template_part( 'template-parts/content', get_post_type() );

							endwhile;

							the_posts_navigation();

						else :

							get_template_part( 'template-parts/content', 'none' );

						endif;
						?>

					</main><!-- #main -->
				</div><!-- #primary -->
			</div>
		</div>
		<div class="col-md-3 blog-right">
			<?php
				// dynamic_sidebar( 'right-sidebar' );
				get_sidebar();
			?>
		</div>
		<div class="clearfix"> </div>
	</div>
</div>
<?php
get_footer();
