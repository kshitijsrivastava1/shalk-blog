<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SHALK_Blog
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="blog-leftl">
		<header class="entry-header">
			<?php
			// if ( is_singular() ) :
			// 	the_title( '<h1 class="entry-title">', '</h1>' );
			// else :
			// 	the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			// endif;
			if ( 'post' === get_post_type() ) :
				?>
				<div class="entry-meta">
					<!-- <h4>December <span>31</span></h4> -->
					<h4>
						Posted On
						<?php
							shalk_blog_posted_on();
						?>
					</h4>
				</div><!-- .entry-meta -->
			<?php endif; ?>
		</header><!-- .entry-header -->
		<!-- <a href="#"><i class="glyphicon glyphicon-tags" aria-hidden="true"></i>10</a> -->
	</div>
	<div class="blog-leftr">
		<?php
			if ( !is_singular() ) :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			endif;
		?>
		<?php shalk_blog_post_thumbnail(); ?>
		<?php
			if ( is_singular() ) :
		?>
			<div class="admin-text">
				<footer class="entry-footer">
					<?php shalk_blog_entry_footer(); ?>
				</footer><!-- .entry-footer -->
			</div>
			<div class="clearfix"> </div>
		<?php
			endif;
		?>
		<div class="entry-content">
			<?php
			if (is_singular( )) :
				the_content();
			else :
				the_excerpt( sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'shalk-blog' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				) );
			endif;

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'shalk-blog' ),
				'after'  => '</div>',
			) );
			?>
		</div><!-- .entry-content -->
		<?php
			if ( is_singular() ) :
		?>
			<ul>
				<li>
					<a href="<?php esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><i class="glyphicon glyphicon-user" aria-hidden="true"></i>
						<?php
							echo esc_html( get_the_author() );
						?>
					</a>
				</li>
				<li><a href="#"><i class="glyphicon glyphicon-tags" aria-hidden="true"></i>0 Tages</a></li>
				<li><a href="#"><i class="glyphicon glyphicon-comment" aria-hidden="true"></i>10 Comments</a></li>
			</ul>
		<?php
			endif;
		?>
	</div>
	<div class="clearfix"> </div>
	<?php
		if ( is_singular() ) :
	?>
		<div class="admin-text">
				<h5>
					Written
					<?php
						shalk_blog_posted_by();
					?>
				</h5>
				<div class="admin-text-left">
					<a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon1.png" alt=""/></a>
				</div>
				<div class="admin-text-right">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,There are many variations of passages of Lorem Ipsum available, 
					sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					<span>
						View all posts 
						<a href="<?php esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>">
						<?php
							esc_html( shalk_blog_posted_by() );
						?>
						</a>
					</span>
				</div>
				<div class="clearfix"> </div>
		</div>
	<?php
		endif;
	?>
</article><!-- #post-<?php the_ID(); ?> -->
