<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SHALK_Blog
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
				$latest_post_args = array(
										'numberposts'	=> 5,
										'orderby'		=> 'date',
								        'order'			=> 'DESC',
								        'post_type'		=> 'post',
								        'post_status'	=> 'publish',
								        '$post'			=> 0,
									);
				$latest_posts = get_posts( $latest_post_args );
			?>

		<!-- after-header -->
			<div class="after-header">
				<div class="container">
					<div class="move-text">
						<div class="breaking_news">
							<h2>Latest Post</h2>
						</div>
						<div class="marquee">
							<?php
								$step = 1;
								foreach ($latest_posts as $key => $post) {
									if ($step >= 3) {
										break;
									}
							?>
									<div class="marquee<?php echo $step; ?>"><a class="breaking" href="<?php echo get_permalink( $post->ID ); ?>"><?php echo $post->post_excerpt !="" ? $post->post_excerpt : substr(strip_tags($post->post_content) , 0 , 100); ?></a></div>
							<?php
									$step++;
								}
							?>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
						<!-- <script type="text/javascript" src="js/jquery.marquee.js"></script> -->
						<script>
						  $('.marquee').marquee({ pauseOnHover: true });
						  //@ sourceURL=pen.js
						</script>
					</div>
			<!-- news-and-events -->
				<div class="news">
					<div class="news-grids">
						<div class="col-md-8 news-grid-left">
							<h3>latest news & events</h3>
							<ul>
								<?php
									foreach ($latest_posts as $key => $post) {
										$post_thumbnail = get_the_post_thumbnail_url( $post->ID, 'post-thumbnail' );
										$post_thumbnail = $post_thumbnail != "" ? $post_thumbnail : wp_get_attachment_image_url( get_theme_mod( 'custom_logo' ) , 'full' );

								?>
										<li>
											<div class="news-grid-left1">
												<img src="<?php echo $post_thumbnail; ?>" alt=" " class="img-responsive" />
											</div>
											<div class="news-grid-right1">
												<h4><a href="<?php echo get_post_permalink( $post->ID ); ?>">
													<?php
														echo $post->post_title;
													?>
												</a></h4>
												<h5>By 
													<a href="#">
														<?php
															echo esc_html( get_the_author_meta( 'display_name', $user_id = $post->post_author ) );
														?>
													</a> 
													<label>|</label> <i><?php echo date('d-m-Y' , strtotime($post->post_date)); ?></i></h5>
												<p>
													<?php 
														the_excerpt( sprintf(
															wp_kses(
																/* translators: %s: Name of current post. Only visible to screen readers */
																__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'shalk-blog' ),
																array(
																	'span' => array(
																		'class' => array(),
																	),
																)
															),
															get_the_title()
														) );
													 ?>
												 </p>
											</div>
											<div class="clearfix"> </div>
										</li>
								<?php
									}
								?>
							</ul>
						</div>
						<div class="col-md-4 news-grid-right blog-right">
							<?php
							get_sidebar();
							?>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
			<!-- //news-and-events -->
				</div>
			</div>
		<!-- //after-header -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
// get_sidebar();
get_footer();
