<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package SHALK_Blog
 */

get_header();
?>
<div class="container">
	<div class="single-grid">
		<div class="col-md-9 blog-left">
			<div class="blog-left-grid">
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
						<?php
						while ( have_posts() ) :
							the_post();

							get_template_part( 'template-parts/content', get_post_type() );

							// the_post_navigation();

							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;

						endwhile; // End of the loop.
						?>	
					</main><!-- #main -->
				</div><!-- #primary -->
			</div>
		</div>
		<div class="col-md-3 blog-right">
			<?php
				// dynamic_sidebar( 'right-sidebar' );
				get_sidebar();
			?>
			<!-- <div class="recent">
				<h3>Recent Comments</h3>
				<div class="recent-grids">
					<div class="recent-grid">
						<div class="wom">
							<a href="#"><img src="images/6.jpg" alt=" " class="img-responsive" /></a>
						</div>
						<div class="wom-right">
							<h4><a href="#">Integer rutrum ante eu</a></h4>
							<p>Mauris fermentum dictum magna. Sed laoreet aliquam leo. 
								Ut tellus dolor, dapibus eget.</p>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
			</div> -->
			<!-- <div class="footer-top-grid1">
				<h3>Recent Tags</h3>
				<ul class="tag2">
					<li><a href="#">awesome</a></li>
					<li><a href="#">strategy</a></li>
					<li><a href="#">development</a></li>
				</ul>
				<ul class="tag2">
					<li><a href="#">css</a></li>
					<li><a href="#">photoshop</a></li>
					<li><a href="#">photography</a></li>
					<li><a href="#">html</a></li>
				</ul>
				<ul class="tag2">
					<li><a href="#">awesome</a></li>
					<li><a href="#">strategy</a></li>
					<li><a href="#">development</a></li>
				</ul>
				<ul class="tag2">
					<li><a href="#">css</a></li>
					<li><a href="#">photoshop</a></li>
					<li><a href="#">photography</a></li>
					<li><a href="#">html</a></li>
				</ul>
				<ul class="tag2">
					<li><a href="#">awesome</a></li>
					<li><a href="#">strategy</a></li>
					<li><a href="#">development</a></li>
				</ul>
			</div> -->
			<!-- <div class="poll">
				<h3>Poll</h3>
					<div class="progress p">
					  <div class="progress-bar bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
						60%
					  </div>
					</div>
					<div class="progress p">
					  <div class="progress-bar bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">
						80%
					  </div>
					</div>
					<div class="progress p">
					  <div class="progress-bar bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 90%;">
						90%
					  </div>
					</div>
					<div class="progress p">
					  <div class="progress-bar bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">
						40%
					  </div>
					</div>
			</div> -->
		</div>
		<div class="clearfix"> </div>
	</div>
</div>

<?php
get_footer();
