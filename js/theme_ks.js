jQuery = jQuery;

jQuery(document).on('load', function(event) {
	event.preventDefault();
	shalk_blog_set_background_img();
	/* Act on the event */
});
jQuery(window).on('load', function(event) {
	shalk_blog_set_background_img();
	/* Act on the event */
})

function shalk_blog_set_background_img(){
    jQuery.each(jQuery('div'), function() {
    	var data_bgks = jQuery(this).attr("data-bgks");
        if ( data_bgks && jQuery(this).offset().top < (jQuery(window).scrollTop() + jQuery(window).height() + 100) ) {
            var source = jQuery(this).data('bgks');
            jQuery(this).css({
              background: "url('"+source+"')"
            });
            // jQuery(this).removeAttr('data-src');
        }
    })
}